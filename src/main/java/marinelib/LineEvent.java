package marinelib;

import java.util.EventObject;

public class LineEvent extends EventObject
{
	private String line;
	private PortIdentifier portId;

	public LineEvent(Object source, String line, PortIdentifier portId) 
	{
		super(source);
		this.line = line;
		this.portId = portId;
	}
	
	public String getLine()
	{
		return line;
	}

	public PortIdentifier getPortId ()
	{
		return portId;
	}

}

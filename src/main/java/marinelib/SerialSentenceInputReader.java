package marinelib;

import java.io.BufferedReader;
import java.io.IOException;


/**
 * A LineReader Thread splits the input from a reader into single lines by 
 * lineseperator <cr> <lf>. After a complete line was read, a listeners will 
 * be notified b a LineEvent.
 * @author Jole
 */
public class SerialSentenceInputReader extends RunnableSentenceInputReader
{
	final BufferedReader reader;					// this will be the input
	
	/**
	 * A LineReader always needs a reader as input. Also the constructor
	 * will create a thread for this.
	 * @param reader the input to be read
	 */
	public SerialSentenceInputReader(BufferedReader reader)
	{
		super("SerialInput");
		this.reader = reader;
	}
	

	@Override
	protected LineEvent receive()
	{
			try 
			{
				String line  = reader.readLine();
				return new LineEvent(this, line, new PortIdentifier("Seriell"));
			}
			catch (IOException e) {
				//System.out.println(e.getMessage());
			}
			return null;	
	}

}

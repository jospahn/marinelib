package marinelib;

import marinelib.listener.LineListener;


/**
 * A LineReader Thread splits the input from a reader into single lines by 
 * lineseperator <cr> <lf>. After a complete line was read, a listeners will 
 * be notified b a LineEvent.
 * @author Jole
 */
public abstract class RunnableSentenceInputReader extends SentenceInputReader implements Runnable
{
	private final Thread thread;
	private boolean isStarted = false;		// keeps the thread Running
	
	public RunnableSentenceInputReader(String threadName)
	{
		this.thread = new Thread (this, threadName);
	}
	

	/**
	 * Start the LineListener thread. When running it will chekc for new 
	 * characters each time invoked.
	 */
	@Override
	public void start()
	{
		if (thread.isAlive() == false)
		{
			isStarted = true;
			thread.start();
		}
	}

	/**
	 * Stop the line listener thread. The thread will die when invoke the
	 * next time (So it will be called one time more after this method call)
	 */ 
	@Override
	public void stop()
	{
		isStarted = false;
	}

	@Override
	public void readLine()
	{
		// not needed here
	}

	@Override
	public void run()
	{
		while (isStarted)	// keep alive or let it die	
		{
			LineEvent lineEvent = receive();

			if (lineEvent != null)
			{
				//System.out.println("marinelib.RunnableSentenceInputReader.run(): " + lineEvent.getLine());
				notifyListeners(LineListener.class, lineEvent);
			}
			
			/* Make sure thread can be stopped even it is
			 * stuck in this loop */
			if (isStarted == false)
				return;

		}
	}

	@Override
	public boolean isStarted()
	{
		return isStarted;
	}

	abstract protected LineEvent receive();

}

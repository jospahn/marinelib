/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import marinelib.listener.SentenceListener;

/**
 * Thie class is a helper to handle the registration and deregistartion
 * of LineListeners to a LineParser. It provides storage of the Listener Type
 * and if it is enabled or not as well as a method to apply the filter.
 * By this it is possible to add or remove a Listener by the same method and
 * use i.e. a Checkbox as boolean input.
 * @author JOS
 */
public class SentenceFilter
{
	private String sid;
	private boolean enabled;	// To be added or not to be	
	
	/**
	 * Constructs a filter with for a given LineListener Type.
	 * @param enabled 
	 */
	public SentenceFilter(String sid, boolean enabled)
	{
		this.sid = sid;
		this.enabled = enabled;
	}


	public void apply (SentenceListener listener, SentenceParser parser)
	{
		if (enabled == true)
			parser.addSentenceListener(listener, sid);
		else
			parser.removeSentenceListener(listener, sid);
	}

}

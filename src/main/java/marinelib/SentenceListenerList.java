/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import java.util.ArrayList;
import marinelib.listener.SentenceListener;

/**
 *
 * @author Jole
 */
public class SentenceListenerList
{
	private ArrayList<ListenerSid> listenerList = new ArrayList<>();

	private int cursor = -1;

	public void add (SentenceListener listener, String sid)
	{
		ListenerSid listenerSid = new ListenerSid(listener, sid);
		
		if (listenerList.contains(listenerSid))
			return;
	
		listenerList.add(listenerSid);
	}

	public void remove (SentenceListener listener, String sid)
	{
		listenerList.remove(new ListenerSid(listener, sid));
	}

	public void resetCursor()
	{
		cursor = -1;
	}

	public boolean hasNext()
	{
		return (++cursor < listenerList.size());
	}

	public boolean hasNext(String sid)
	{
		while (hasNext())
		{
			String sidInList = listenerList.get(cursor).sid;
			if (sidInList == null || sidInList.equals(sid))
				return true;
		}
		return false;
	}

	public SentenceListener getSentenceListener()
	{
		return (listenerList.get(cursor).listener);
	}

	private class ListenerSid 
	{
		final SentenceListener listener;
		final String sid;

		ListenerSid (SentenceListener listener, String sid)
		{
			this.listener = listener;
			this.sid = sid;
		}

		@Override
		public boolean equals (Object o)
		{
			if (!(o instanceof ListenerSid))
				return false;

			ListenerSid that =(ListenerSid) o;
			
			// If both sid==null, check if also listener is the same
			if (this.sid == null && that.sid == null)
				return  (this.listener.equals(that.listener));

			// avoid that sid.equals is called with null, string 
			if (this.sid == null || that.sid == null)
				return false;
			
			if (this.sid.equals(that.sid))
				return  (this.listener.equals(that.listener));

			return false;
		}
	}
}

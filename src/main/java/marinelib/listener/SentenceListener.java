package marinelib.listener;

import java.util.EventListener;
import marinelib.SentenceEvent;

public interface SentenceListener extends EventListener
{
	void update (SentenceEvent e);
}

package marinelib.listener;

import java.util.EventListener;
import marinelib.LineEvent;

public interface LineListener extends EventListener
{
	void update (LineEvent e);
}

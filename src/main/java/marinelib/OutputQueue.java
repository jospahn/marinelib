/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import java.lang.Thread.State;
import java.util.concurrent.ConcurrentLinkedQueue;
import marinelib.listener.SentenceListener;
import marinelib.util.CheckSum;

/**
 *
 * @author Jole
 */
public abstract class OutputQueue implements Runnable, SentenceListener
{
	ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
	Thread thread;
	private boolean started = false;

	public OutputQueue()
	{
		this.thread = new Thread(this, "OutputQueue");
	}

	public void start()
	{
		thread.start();
		this.started=true;
	}

	public void stop()
	{
		this.started = false;
	}

	@Override
	public void update(SentenceEvent e)
	{
		String line = e.getSentence().toString();
		if (this.started) {
			if (line != null) {
				//int checkSum = CheckSum.calcChecksum(line.substring(1, line.indexOf('*')));
				//line = line + Integer.toHexString(checkSum).toUpperCase() + "\r\n";
				line = line + CheckSum.getHexChecksum(line) + "\r\n";
				queue.add(line);
			}
			if (this.thread.getState() == State.TERMINATED) {
				this.thread = new Thread(this, "OutputQueue");
				this.thread.start();
			}
		}
	}

	@Override
	public void run()
	{
			String line;
			for (int i = 0; i<queue.size(); i++ )
			while ((line=queue.poll()) != null  ) 
			{
				send(line);
			}
	}

	abstract protected void send (String line);

	public boolean isStarted ()
	{
		return this.started;
	}

}
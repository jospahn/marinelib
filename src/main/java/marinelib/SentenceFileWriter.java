/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import java.io.BufferedWriter;
import java.io.IOException;
import marinelib.listener.LineListener;

/**
 *
 * @author Jole
 */
public class SentenceFileWriter implements LineListener
{
	BufferedWriter bufferedWriter;
	
	public SentenceFileWriter(BufferedWriter bufferedWriter)
	{
		this.bufferedWriter = bufferedWriter;
	}


	@Override
	public void update(LineEvent e)
	{
		try
		{
			bufferedWriter.write(e.getLine() + "\n");
		} catch (IOException ex)
		{
		}
	}

	public void close() throws IOException
	{
		bufferedWriter.close();
	}
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Jole
 */
public class SerialOutputQueue extends OutputQueue 
{
	OutputStream out;
	
	public SerialOutputQueue(OutputStream out)
	{
		this.out = out;
	}


	@Override
	protected void send(String line)
	{
		try
		{
			out.write(line.getBytes());
		} catch (IOException ex)
		{
			System.out.println("gui.OutputBuffer.run(): " + ex.getClass());
			System.out.println(ex.getMessage());
		}
	}
}
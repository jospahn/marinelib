/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.udp;

import java.net.InetAddress;
import marinelib.LineEvent;
import marinelib.PortIdentifier;

/**
 *
 * @author jan-olespahn
 */
public class UdpEvent extends LineEvent
{   
    InetAddress srcIp;
    InetAddress destIp;
    public int destPort;

    public UdpEvent(Object source, String line, 
            InetAddress srcIp, InetAddress destIp, int destPort) 
    {
        super(source, line, new PortIdentifier("UDP"));
        this.destIp = destIp;
        this.destPort = destPort;
        this.srcIp = srcIp;
    }
    
    
  
}

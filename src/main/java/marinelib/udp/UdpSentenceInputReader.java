/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.udp;

import java.io.IOException;
import java.net.*;

import marinelib.LineEvent;
import marinelib.PortIdentifier;
import marinelib.RunnableSentenceInputReader;

/**
 *
 * @author Jole
 */
public class UdpSentenceInputReader extends RunnableSentenceInputReader
{
	private MulticastSocket socket;
	private InetAddress groupIp;
	private int port;

	public UdpSentenceInputReader(InetAddress groupIp, int port)
	{
		super("UdpInput");

		this.port = port;
		this.groupIp = groupIp;

		try {
			this.socket = new MulticastSocket(port);
			System.out.println("Socket created on " + socket.getLocalSocketAddress());
		} catch (IOException e) {
			System.out.println("IOException creating Socket: " + e.getMessage());
		}
		try {
			socket.joinGroup(this.groupIp);
		} catch (IOException e) {
			System.out.println("IOException joining Group: " + e.getMessage());
		}
	}

	public UdpSentenceInputReader(InetAddress groupIp, int port, InetAddress nifIp)
	{
	    this(groupIp, port);
		try {
			socket.setInterface(nifIp);
		} catch (SocketException e) {
		    System.out.println("Exception setting nif:" + e.getMessage());
		}
		try {
			socket.joinGroup(this.groupIp);
		} catch (IOException e) {
			System.out.println("IOException joining Group: " + e.getMessage());
		}
	}


	@Override
	protected LineEvent receive()
	{
		try
		{
			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
			socket.receive(packet);
			String line = new String(packet.getData(), 0, packet.getLength()-1);
			return new LineEvent(this, line, new PortIdentifier("UDP "
				+ groupIp.toString() + ":" + port ));
		} catch (IOException ex)
		{
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Override
	public void stop()
	{
		super.stop();
		if (socket != null)
			socket.close();                
	}
	
}

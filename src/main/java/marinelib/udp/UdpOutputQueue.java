/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.udp;

import java.io.IOException;
import java.net.*;

import marinelib.OutputQueue;

/**
 *
 * @author Jole
 */
public class UdpOutputQueue extends OutputQueue
{
	private MulticastSocket socket;
	private InetAddress groupIp;
	private int port;

	public UdpOutputQueue(InetAddress groupIp, int port) throws IOException
	{
		this.groupIp = groupIp;
		this.port = port;
		this.socket = new MulticastSocket();
		System.out.println(this.socket.getInterface());

	}

	public UdpOutputQueue(InetAddress groupIp, int port, InetAddress nifIp) throws IOException
	{
		this(groupIp, port);
		this.socket.setInterface(nifIp);
		System.out.println(this.socket.getInterface());
	}

	@Override
	protected void send(String line)
	{
		try
		{
			DatagramPacket packet = new DatagramPacket(
					line.getBytes(), line.length(), groupIp,  port);
			System.out.println("DatagrammPacket created");
			//System.out.println("Socket nif: " + socket.getLocalAddress());
			socket.send(packet);
			System.out.println("DatagrammPacket sent");
		} catch (UnknownHostException ex){
            System.err.println("Unknown Host");
		} catch (IOException ex)
		{
			System.err.println("IOException: " +  ex.getMessage());
		}
	}

	@Override
	public void stop()
	{
		socket.close();
		super.stop();
	}
}

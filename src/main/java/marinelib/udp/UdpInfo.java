/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.udp;

import java.net.InetAddress;

/**
 * If Nmea data are transmitted via UDP, information about IP adresses and ports
 * belonging to a transmission can be maintained in here.
 * @author jan-olespahn
 */
public class UdpInfo 
{   
    InetAddress srcIp;
    InetAddress destIp;
    int destPort;

    public UdpInfo(InetAddress srcIp, InetAddress destIp, int destPort) 
    {
        this.destIp = destIp;
        this.destPort = destPort;
        this.srcIp = srcIp;
    }
    
    
  
}

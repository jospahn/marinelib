/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

/**
 * To be thrown while parsing an NMEA sentence when data fields are empty or
 * or when calling getter on missing values on Nmea Data. 
 * @author jan-olespahn
 */
public class EmptyDataFieldException extends RuntimeException
{
    /**
     * Constructor
     * 
     * @param message 
     */
    public EmptyDataFieldException(String message)
    {
        super(message);
    }
    
    /**
     * Constructor
     * 
     * @param message
     * @param cause 
     */
    public EmptyDataFieldException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
}

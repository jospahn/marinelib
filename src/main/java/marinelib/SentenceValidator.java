/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

import marinelib.util.CheckSum;

/**
 *
 * @author Jole
 */
public class SentenceValidator
{
	private String header;
	private String payload;
	private int receivedCheckSum;
	private boolean isSentence = false;
	private boolean hasValidCheckSum = false;

	public SentenceValidator(String string)
	{
		if (string == null || string.length() < 7)
		{
			return;
		}
		
		int indexOfStar;
		/* Chekc if string seems to have NMEA header */
		if (string.charAt(0) != '$' || string.charAt(6) != ',')
		{
			System.out.println("marinelib.SentenceValidator.<init>(): HeaderFailure");
			return;
		}

		indexOfStar = string.indexOf('*');

		/* Check if there is a NMEA tail */
		if (indexOfStar < 8)
		{
			System.out.println("marinelib.SentenceValidator.<init>(): Tail Failure in " + string);
			return;
		}
		
		header = string.substring(0, 6).toUpperCase();
		payload = string.substring(7, indexOfStar).toUpperCase();

		try 
		{
			receivedCheckSum = Integer.parseInt ( string.substring
									( indexOfStar + 1, indexOfStar + 3 ), 16 );
		}
		catch ( StringIndexOutOfBoundsException | NumberFormatException ex)
		{
                        System.out.println("marinelib.SentenceValidator.<init>(): Exception while Tail parsing");
                        System.out.println(ex.getMessage());
		}

		if (receivedCheckSum == CheckSum.calcChecksum(string.substring(1, indexOfStar)))
		{
			hasValidCheckSum = true;
		}

		isSentence = true;
	}

	public boolean isSentence()
	{
		return isSentence;
	}

	public boolean hasValidCheckSum ()
	{
		return hasValidCheckSum;
	}

	public String getSentenceId ()
	{
		if (header.charAt(0) == 'P')
		{
			return header;
		}

		return header.substring(3);
	}

	public String getTalkerId ()
	{
		return header.substring(1, 3);
	}
        
        public String getPayload ()
        {
            return payload;
        }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;


/**
 * Enumeration of talker indetifiers.
 * Source: http://www.catb.org/gpsd/NMEA.html#_talker_ids
 * Some of the listed TalkerIdentifiers are not in this enum, as they are
 * obsolete and/or will mostlikely never be seen by this software.
 * Also some may be added additionally when seen on data while testing.
 * @author jan-olespahn
 */
public enum TalkerIdentifiers 
{
    AB ("Indipendent AIS Base Station"), 
    AD ("Dependent AIS Base Station"), 
    AG ("Autopilot - General"), 
    AP ("Autopilot - Magnetic"), 
    BN ("BNWAS"),
    CD ("Communications: DSC"),
    CS ("Communications: satellite"),
    CT ("Communications: MF/HF"),
    CV ("Communications: VHF"),
    CX ("Communications: scanner"),
    DF ("Direction Finder"),
    DU ("Duplex Repeater station"),
    EC ("EDCIS"),
    EP ("EPIRB"),
    ER ("Engine Room Monitoring"),
    GP ("GPS"),
    HC ("Compass magnetic"),
    HE ("Gyro northseeking"),
    HN ("Gyro non-northseeking"),
    II ("Integrated instrumentation"),
    IN ("Integrated navigation"),
    NL ("Navigation light controller"),
    RA ("Radar"),
    SD ("Sounder depth"),
    SN ("Electronic positioning system"),
    SS ("Sounder scanning"),
    TI ("Turn rate indicator"),
    UP ("Microprocessor controller"),
    VD ("Velocity sensor: doppler"),
    VM ("Velocity sensor: speed log"),
    VW ("Velocity sensor: speed log"),
	WI ("Weather Instrument"),
    ZA ("atomic clock"),
    ZC ("chronometer"),
    ZQ ("quartz clock"),
    ZV ("radio update clock");
	
    private final String description;

    private TalkerIdentifiers(String description ) 
	{
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

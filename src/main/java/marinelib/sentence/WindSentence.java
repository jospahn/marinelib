package marinelib.sentence;
import marinelib.TalkerIdentifiers;
import marinelib.data.WindData;
import marinelib.data.WindSupplier;

public abstract class WindSentence extends AbstractSentence implements WindSupplier
{
	WindData wind = new WindData();
	
	/**
	 * Sets talker id, sentence ID and payload (which then will be split by super class)
	 * @param tid
	 * @param sid
	 * @param payload 
	 */
	WindSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
	}
	
	WindSentence(TalkerIdentifiers tid, String sid, int noOfFields)
	{
		super(tid, sid, noOfFields);
	}
        
	/**
	 * Parse the wind speed in from fields[] at given index with given unit.
	 * @param speedIdx
	 * @param unit K=km/h, M=m/s, N=knots, S=mph
	 */
	void parseWindSpeed (int speedIdx, char unit)
	{
		try
		{
			wind.parseSpeed(fields[speedIdx], unit);
		}
		catch (IllegalArgumentException | ArrayIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection(): "
			//			+ ex.getMessage());
		}
	}

	/**
	 * Parse the wind speed in from fields[] at given index and parse unit from given Field idx.
	 * @param speedIdx
	 * @param unitIdx 
	 */
	void parseWindSpeed (int speedIdx, int unitIdx)
	{
		try
		{
			char unit  = fields[unitIdx].charAt(0);
			parseWindSpeed(speedIdx, unit);
		}
		catch (ArrayIndexOutOfBoundsException 
				| StringIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection()"
			//			+ ex.getMessage());	
		}
	}
	
	/**
	 * Parse the wind direction from given field Index, set true or relative
	 * @param dirIdx
	 * @param tr T=true Wind, R=relative Wind
	 */
	void parseWindDirection (int dirIdx, char tr)
	{
		try
		{
			wind.parseDirection(fields[dirIdx]);
		}
		catch (IllegalArgumentException | ArrayIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection(): "
			//			+ ex.getMessage());
		}
		if (tr == 'T')
			wind.setTrueWind(true);
		//else if (tr != 'R')
			//System.out.println("marinelib.sentence.WindSentence.parseWindDirection(): unknown True/Relative Flag read");
	}

	/**
	 * Parse the wind direction from given field Index and parse if true wind or relative wind from given idx.
	 * @param dirIdx
	 * @param trIdx
	 */
	void parseWindDirection (int dirIdx, int trIdx)
	{
		try
		{
			char tr  = fields[trIdx].charAt(0);
			parseWindDirection(dirIdx, tr);
		}
		catch (ArrayIndexOutOfBoundsException 
				| StringIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection()"
			//			+ ex.getMessage());	
		}
	}
	
	/**
	 * Parse the wind angle relative to heading line from given field Idx.
	 * @param angleIdx 
	 */
	void parseWindAngle (int angleIdx)
	{
		try
		{
			wind.parseAngle(fields[angleIdx]);
		}
		catch (IllegalArgumentException | ArrayIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection(): "
			//			+ ex.getMessage());
		}
	}

	/**
	 * Parse the wind angle relative to heading line from given field Idx and parse right or left from given field Idx.
	 * @param angleIdx 
	 * @param sideIdx 
	 */
	void parseWindAngle (int angleIdx, int sideIdx)
	{
		parseWindAngle(angleIdx);
		try
		{
			char side  = fields[sideIdx].charAt(0);
			wind.parseSide(side);
		}
		catch (ArrayIndexOutOfBoundsException 
				| IllegalArgumentException
				| StringIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.WindSentence.parseWindSpeedDirection()"
			//			+ ex.getMessage());	
		}
	}

	@Override
	public WindData getWind() 
	{
		return wind;
	}

	@Override
	public String getInfoString ()
	{
		return	tid + sid + ": " + wind.toString();
	}
}

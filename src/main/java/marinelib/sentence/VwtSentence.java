/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 * relative wind Speed and Angle.
 * @author spahn
 */
public class VwtSentence extends VwrSentence
{
	public static final String SID = "VWT";
	
	public VwtSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);
		wind.setTrueWind(true);
	}
	
}

/*
VWT - True Wind Speed and Angle

         1  2  3  4  5  6  7  8 9
         |  |  |  |  |  |  |  | |
 $--VWR,x.x,a,x.x,N,x.x,M,x.x,K*hh<CR><LF>

 Field Number: 
  1) Wind direction magnitude in degrees
  2) Wind direction Left/Right of bow
  3) Speed
  4) N = Knots
  5) Speed
  6) M = Meters Per Second
  7) Speed
  8) K = Kilometers Per Hour
  9) Checksum 
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.EmptyDataFieldException;
import marinelib.TalkerIdentifiers;
import marinelib.data.*;

import java.util.HashMap;

/**
 * Dual Ground/Water Speed
 * Todo: not yet tested
 * @author jos
 */
public class VtgSentence extends SpeedSentence implements CourseSupplier
{
	public static final String SID = "VTG";

	/* Index of data fields */
	static final int TRACK_MADE_GOOD_TRUE_IDX = 0; 			//
	static final int TRACK_MADE_GOOD_MAGNETIC_IDX = 2; 			//
	static final int SPEED_N_IDX = 4; 			//
	static final int SPEED_K_IDX = 6; 			//

    HashMap<BearingData.ReferenceType, CourseData> courses = new HashMap<>();

	public VtgSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);

		CourseData courseData ;
		try
		{
			courseData = new CourseData();
			courseData.parseString(fields[TRACK_MADE_GOOD_TRUE_IDX]);
			courseData.setReferenceType(BearingData.ReferenceType.TRUE);
			courses.put(BearingData.ReferenceType.TRUE, courseData);
		} catch (IllegalArgumentException Ex){}
		try {
			courseData = new CourseData();
			courseData.parseString(fields[TRACK_MADE_GOOD_MAGNETIC_IDX]);
			courseData.setReferenceType(BearingData.ReferenceType.MAGNETIC);
			courses.put(BearingData.ReferenceType.MAGNETIC, courseData);
		} catch (IllegalArgumentException Ex){}


		super.parseSpeed(SPEED_N_IDX, SpeedData.SpeedType.GROUND_SPEED);
		// ToDO: in case a sentence only transmits speed in Km/h, will Knots field be empty, or will it be 0?
		if (speeds.isEmpty())				// In case knots was empty, try to parse km/h
			super.parseSpeed(SPEED_K_IDX, SpeedData.SpeedType.GROUND_SPEED, 'K');
	}

	public VtgSentence(TalkerIdentifiers tid, SpeedData speedData)
	{
		super(tid, SID, 8);
	}

	@Override
	public CourseData getCourseData() {
		CourseData courseData;

		for (BearingData.ReferenceType type : BearingData.ReferenceType.values())
		{
			courseData = courses.get(type);
			if (courseData != null)
				return courseData;
		}

		throw new EmptyDataFieldException("No course data available");
	}
}

/*
VTG Track Made Good and Ground Speed
123456789
|||||||||
$--VTG,x.x,T,x.x,M,x.x,N,x.x,K*hh
1) Track Degrees
2) T = True
3) Track Degrees
4) M = Magnetic
5) Speed Knots
6) N = Knots
7) Speed Kilometers Per Hour
8) K = Kilometres Per Hour
9) Checksum
*/


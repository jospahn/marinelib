/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 * In this class all classes for specific NmeaSentences shall be listed.
 * For each known sentence type a XXXSentence object can be retireved, the
 * SID can be retrieved or the Class type for the XxxSentence can be queried.
 * @author Jole
 */
public enum SentenceTypesEnum
{

		DPT(DptSentence.class),
		GGA(GgaSentence.class), 
		GLL(GllSentence.class),
		HDG(HdgSentence.class),
		HDM(HdmSentence.class),
		HDT(HdtSentence.class),
		MWV(MwvSentence.class),
		ROT(RotSentence.class),
		VBW(VbwSentence.class),
		VTG(VtgSentence.class),
		VWR(VwrSentence.class),
		VWT(VwtSentence.class);


	private Class c;
	
	private SentenceTypesEnum (Class c)
	{
		this.c = c;
	}

	public AbstractSentence getSentence(TalkerIdentifiers tid, String payload)
	{
		AbstractSentence sentence;
		try
		{
			sentence = (AbstractSentence)
					c.getConstructor(TalkerIdentifiers.class, String.class).newInstance(tid, payload);
			return sentence;
		} catch (Exception ex)
		{
			System.out.println("Exeption  in marinelib.sentence.SentenceTypesEnum.getSentence(): " + ex.getMessage());
		}
		return null;
	}

	public Class getSentenceType ()
	{
		return c;
	}

}

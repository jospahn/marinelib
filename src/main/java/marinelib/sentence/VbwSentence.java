/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.SpeedData;
import marinelib.data.WindData;

/**
 * Dual Ground/Water Speed
 * Todo: not yet tested
 * @author jos
 */
public class VbwSentence extends SpeedSentence
{
	public static final String SID = "VBW";

	/* Index of data fields */
	static final int WATER_SPEED_IDX = 0; 				// Longitudinal Water Speed
	static final int TRANSVERSE_WATER_SPEED_IDX = 1; 	// Transverse Water Speed
	static final int STW_STATUS_IDX = 2; 				// Status of Water Speed (A= valid)
	static final int GROUND_SPEED_IDX = 3; 				// Longitudinal Ground Speed
	static final int TRANSVERSE_GROUND_SPEED_IDX = 4; 	// Transverse Ground Speed
	static final int SOG_STATUS_IDX = 5; 				// Status of Ground Speed (A= valid)


	static final char STATUS_VALID = 'A';

	public VbwSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);

		if (fields[STW_STATUS_IDX].charAt(0) == STATUS_VALID)
		{
			parseSpeed(WATER_SPEED_IDX, SpeedData.SpeedType.LONGITUDINAL_WATER_SPEED);
			parseSpeed(TRANSVERSE_WATER_SPEED_IDX,  SpeedData.SpeedType.TRANSVERSE_WATER_SPEED);
		}

		if (fields[SOG_STATUS_IDX].charAt(0) == STATUS_VALID)
		{
			parseSpeed(GROUND_SPEED_IDX, SpeedData.SpeedType.LONGITUDINAL_GROUND_SPEED);
			parseSpeed(TRANSVERSE_GROUND_SPEED_IDX, SpeedData.SpeedType.TRANSVERSE_GROUND_SPEED);
		}
	}

	public VbwSentence(TalkerIdentifiers tid, SpeedData speedData, SpeedData... moreSpeedData )
	{
		super(tid, SID, 6);
	}

}

/*
VBW Dual Ground/Water Speed
1234567
|||||||
$--VBW,x.x,x.x,A,x.x,x.x,A*hh
1) Longitudinal water speed, "-" means astern
2) Transverse water speed, "-" means port
3) Status, A = data valid
4) Longitudinal ground speed, "-" means astern
5) Transverse ground speed, "-" means port
6) Status, A = data valid
7) Checksum

*/
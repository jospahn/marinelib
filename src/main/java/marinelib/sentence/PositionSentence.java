/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.data.TimeData;
import marinelib.data.TimeSupplier;

/**
 * Nmea Sentences mainly used for GPS position as GGA should inherit from this class.
 * Supports parsing of position and time and implemenst methods for implemented Interfaces
 * @author jos
 */
abstract public class PositionSentence extends AbstractSentence
		implements PositionSupplier, TimeSupplier
{
	
	PositionData position = new PositionData();
	TimeData time = new TimeData();
	
	/**
	 * 
	 * @param tid
	 * @param sid
	 * @param payload 
	 */
	PositionSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
	}

	/**
	 * Parse the position from the payload fields withnthe given filed numbers
	 * @param latIdx
	 * @param latHemiIdx
	 * @param lonIdx
	 * @param lonHemiIdx 
	 */
	void parsePosition (	int latIdx, 
							int latHemiIdx, 
							int lonIdx, 
							int lonHemiIdx) 
	{
		try 
		{
			position.parseLat(fields[latIdx]);
			position.parseHemi(fields[latHemiIdx].charAt(0));
			position.parseLon(fields[lonIdx]);
			position.parseHemi(fields[lonHemiIdx].charAt(0));
		} 
		catch (IllegalArgumentException ex)
		{
			System.out.println("marinelib.sentence.PositionSentence.parsePosition(): Exception parsing position");
		}
	}

	/**
	 * Parse the time (usually of position fix) from payload at given field number 
	 * @param timeIdx 
	 */
	void parseTime ( int timeIdx) 
	{
		if (fields[timeIdx] == null)
			return;

		try
		{
			time.parseTime(fields[timeIdx]);
		}
		catch (IllegalArgumentException ex)
		{
			System.out.println("marinelib.sentence.PositionSentence.parsePosition(): Exception parsing time");
		}
	}

	/**
	 * 
	 * @return  position
	 */
	@Override
	public PositionData getPosition()
	{
		return position;
	}

	/**
	 * 
	 * @return time
	 */
	@Override
	public TimeData getTime()
	{
		return time;
	}

	@Override
	public String getInfoString ()
	{
		return	tid + sid + ": "
				+ position.toString() 
				+ " @ "
				+ time.toString() 
				;		
	}
}

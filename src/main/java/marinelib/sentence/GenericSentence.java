/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 *
 * @author jos
 */
public class GenericSentence extends AbstractSentence
{
	
	public GenericSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
	}
	
	@Override
	public String getInfoString ()
	{
		return	tid + sid + ": unknown Sentenceformat";
	}
	
}

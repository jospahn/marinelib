/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 *
 * @author jos
 */
public class HdtSentence extends HdmSentence
{
	public static final String SID = "HDT";

	public HdtSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);
	}
}

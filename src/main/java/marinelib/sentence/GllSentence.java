/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;


/**
 * The NMEA GLL sentence carriyng position, fix time and status flag.
 * @author jos
 */
public class GllSentence extends PositionSentence
{
	public final static String SID = "GLL";

	/**
	 * Used for received sentences, parses data from payload
	 * @param payload
	 */
	public GllSentence(TalkerIdentifiers tid, String payload) 
    {
        super(tid, SID, payload);	
		
		super.parsePosition(0, 1, 2, 3);

		super.parseTime(4);
		
		//Datastatus
		
	}

	/**
	 * 
	 * @return 
	 */
	@Override
	public String getInfoString ()
	{
		return "[" + SID + ": " 
			+ position
			+ " @ " + time
			+ "]"
		;
	}

}

/*
       1       2 3        4 5         6 7
       |       | |        | |         | |
$--GLL,llll.ll,a,yyyyy.yy,a,hhmmss.ss,A*hh<CR><LF>

 Field Number: 
  1) Latitude
  2) N or S (North or South)
  3) Longitude
  4) E or W (East or West)
  5) Universal Time Coordinated (UTC)
  6) Status A - Data Valid, V - Data Invalid , P - Precise
  7) Checksum 

*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 * relative wind Speed and Angle.
 * @author spahn
 */
public class VwrSentence extends WindSentence
{
	public static final String SID = "VWR";
	
	/* Index of data fields */
	static final int ANGLE_IDX = 0;
	static final int LEFT_RIGHT_IDX = 1;
	static final int SPEED_N_IDX = 2;
	static final int UNIT_N_IDX = 3;
	static final int SPEED_M_IDX = 4;
	static final int UNIT_M_IDX = 5;
	static final int SPEED_K_IDX = 6;
	static final int UNIT_K_IDX = 7;

	public VwrSentence(TalkerIdentifiers tid, String payload)
	{
		this(tid, SID, payload);
	}

	VwrSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
		super.parseWindAngle(ANGLE_IDX, LEFT_RIGHT_IDX);
		if (fields[SPEED_M_IDX] != null)
			super.parseWindSpeed(SPEED_M_IDX, UNIT_M_IDX);
		else if (fields[SPEED_K_IDX] != null)
			super.parseWindSpeed(SPEED_K_IDX, UNIT_K_IDX);
		else if (fields[SPEED_N_IDX] != null)
			super.parseWindSpeed(SPEED_N_IDX, UNIT_N_IDX);
	}
}

/*
VWR - Relative Wind Speed and Angle

         1  2  3  4  5  6  7  8 9
         |  |  |  |  |  |  |  | |
 $--VWR,x.x,a,x.x,N,x.x,M,x.x,K*hh<CR><LF>

 Field Number: 
  1) Wind direction magnitude in degrees
  2) Wind direction Left/Right of bow
  3) Speed
  4) N = Knots
  5) Speed
  6) M = Meters Per Second
  7) Speed
  8) K = Kilometers Per Hour
  9) Checksum 
*/
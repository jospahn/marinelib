/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.WindData;

/**
 * Wind Speed and Angle.
 * @author spahn
 */
public class MwvSentence extends WindSentence
{
	public static final String SID = "MWV";

	/* Index of data fields */
	static final int ANGLE_IDX = 0; 	// Wind angle (better direction)
	static final int REFERNCE_IDX = 1; 	// R=Relative  T=True
	static final int SPEED_IDX = 2; 	// Wind speed
	static final int SPEED_UNIT_IDX = 3;// Wind speed units
	static final int STATUS_IDX = 4;	// A = Valid 

	
	static final char STATUS_VALID = 'A';
	
	public MwvSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);
		super.parseWindDirection(0, 1);
		super.parseWindSpeed(2, 3);
	}

	public MwvSentence(TalkerIdentifiers tid, WindData wind)
	{
		super(tid, SID, 5);
		this.wind = wind;

		fields[ANGLE_IDX] = Double.toString(wind.getDirection());
		fields[REFERNCE_IDX] = (wind.isTrueWind() ? "T" : "R"); 
		fields[SPEED_IDX] = Double.toString(wind.getSpeed());
		fields[SPEED_UNIT_IDX] = "M";
		fields[STATUS_IDX] = "A";
	}

}

/*
1 2 3 4 5
| | | | |
$--MWV,x.x,a,x.x,a*hh
1) Wind Angle, 0 to 360 degrees
2) Reference, R = Relative, T = True
3) Wind Speed
4) Wind Speed Units, K/M/N
5) Status, A = Data Valid
6) Checksum
*/
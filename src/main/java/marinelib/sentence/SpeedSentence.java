/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.EmptyDataFieldException;
import marinelib.TalkerIdentifiers;
import marinelib.data.SpeedData;
import marinelib.data.SpeedData.SpeedType;
import marinelib.data.SpeedSupplier;

import java.util.HashMap;

/**
 * Classes represnting speed sentenceses should inherit from this class
 * Todo: not yet tested
 * @author jos
 */
public abstract class SpeedSentence extends AbstractSentence implements SpeedSupplier
{
	HashMap <SpeedData.SpeedType, SpeedData> speeds = new HashMap<>();

	/**
	 * Consructor used for received sentnces
	 * @param tid
	 * @param sid
	 * @param payload
	 */
	SpeedSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
	}

	/**
	 * Constructor used for generated sentences
	 * @param tid
	 * @param sid
	 * @param noOfFields
	 */
	SpeedSentence(TalkerIdentifiers tid, String sid, int noOfFields)
	{
		super(tid, sid, noOfFields);
	}

	/**
	 * Parses speed from the sentence field given by fieldIdx to the speedData in the array given by speedIdx and sets
	 * it's validity to true.
	 * @param fieldIdx from which sentence field shall be parsed?
	 * @param type which type is the speed, i.e. SpeedData.SpeedType.GROUND_SPEED
	 * @param unit meansurement unit of speed, check SpeedData for possible units
	 */
	void parseSpeed (int fieldIdx, SpeedData.SpeedType type, char unit)
	{
	    SpeedData speed = new SpeedData();
		try
		{
			speed.parseString(fields[fieldIdx], unit);
			speed.setValid(true);
			speed.setSpeedType(type);
			speeds.put(type, speed);
		}
		catch (ArrayIndexOutOfBoundsException 
				| IllegalArgumentException
				| StringIndexOutOfBoundsException ex)
		{
			System.out.println(ex);
		}
	}

	void parseSpeed (int fieldIdx, SpeedData.SpeedType type )
	{
		parseSpeed(fieldIdx, type, 'N');
	}

	@Override
	public SpeedData getSpeedData ()
	{
		SpeedData speed;

		for (SpeedType type : SpeedType.values())
		{
			speed = speeds.get(type);
			if (speed != null)
			    return speed;
		}

		throw new EmptyDataFieldException("No speed data available");
	}

	@Override
	public SpeedData getSpeedData(SpeedData.SpeedType type)
			throws EmptyDataFieldException, UnsupportedOperationException
	{
		if (speeds.isEmpty())
			throw new EmptyDataFieldException("No speed available");

		SpeedData speed = speeds.get(type);
		if (speed != null)
			return speed;

		throw new EmptyDataFieldException("No speed of that type available");

	}

	@Override
	public String getInfoString()
	{
		return tid + sid + ": ";
		//ToDo: Add data
	}

}

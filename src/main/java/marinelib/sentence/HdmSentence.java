/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 *  
 * @author jos
 */
public class HdmSentence extends HeadingSentence
{
	public static final String SID = "HDM";

	static final int HEADING_IDX = 0;
	static final int TYPE_IDX = 1;

	public HdmSentence(TalkerIdentifiers tid, String payload)
	{
		this(tid, SID, payload);
	}

	/* This constructor is needed, as HDGSenetnece is extended by HDTSetntence */
	HdmSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
		super.parseHeading(HEADING_IDX);
		super.parseType(TYPE_IDX);
		super.headingToHashMap();
	}
	
}

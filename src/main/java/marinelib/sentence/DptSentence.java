/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.DepthData;

/**
 *
 * @author jos
 */
public class DptSentence extends AbstractSentence
{
	public static final String SID = "DPT";
	
	DepthData depth = new DepthData();
	
	public DptSentence(TalkerIdentifiers tid, String payload)
	{
		super(tid, SID, payload);

		if (fields[0] != null)
			depth.setDepth(Float.parseFloat(fields[0]));

		if (fields.length > 1 && fields[1] != null)
		{
			Float offset = Float.parseFloat(fields[1]);
			if (offset < 0)
			{
				depth.setOffsetKeel(offset);
			}
			else
			{
				depth.setOffsetWater(offset);
			}
		}
	}

	@Override
	public String getInfoString ()
	{
		return tid + sid + ": " + depth;
	}
}

/*
        1   2   3
        |   |   |
 $--DPT,x.x,x.x*hh<CR><LF>

 Field Number: 
  1) Depth, meters
  2) Offset from transducer,
     positive means distance from tansducer to water line
     negative means distance from transducer to keel
  3) Checksum


*/
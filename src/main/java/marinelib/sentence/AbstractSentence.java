/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TagBlock;
import marinelib.TalkerIdentifiers;

/**
 *
 * @author jos
 */
public abstract class AbstractSentence
{
	static final char FIELD_SEPARATOR = ',';
	static final char END_DELIMITER = '*';

	TagBlock tagblock;
	final TalkerIdentifiers tid;
	final String sid;
	final String[] fields;
	final char startDelimiter;
	boolean proprietary = false;

	abstract public String getInfoString ();
	
	/**
	 * Constructor for received sentences.
	 * @param tid
	 * @param sid
	 * @param payload 
	 */
	AbstractSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		this.tid = tid;
		this.sid = sid;
		fields = payload.split(",");
		this.startDelimiter = '$';
	}

	/**
	 * Constructor for building sentences to be sent out
	 * @param tid
	 * @param sid
	 * @param noOfFields 
	 */
	AbstractSentence(TalkerIdentifiers tid, String sid, int noOfFields)
	{
		this.tid = tid;
		this.sid = sid;
		fields = new String[noOfFields];
		this.startDelimiter = '$';
	}
	
	/**
	 * Get the Talker ID
	 * @return 
	 */
	public TalkerIdentifiers getTid ()
	{
		return tid;
	}

	/**
	 * Get the Sentence ID
	 * @return 
	 */
	public String getSid ()
	{
		return sid;
	}

	@Override
	public final String toString ()
	{
		StringBuilder sb = new StringBuilder(80);
		sb.append(startDelimiter).append(tid).append(sid);
		for (String f : fields)
		{
			sb.append(FIELD_SEPARATOR);
			sb.append(f);
		}
		sb.append(END_DELIMITER);
		return sb.toString();
	}
}

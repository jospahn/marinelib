/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.BearingData;

/**
 *  
 * @author jos
 */
public class HdgSentence extends HeadingSentence
{
	public static final String SID = "HDG";

	static final int HEADING_IDX = 0;
	static final int DEVIATION_IDX = 1;
	static final int DEVIATION_EW_IDX = 2;
	static final int VARIATION_IDX = 3;
	static final int VARIATION_EW_IDX = 4;


	public HdgSentence(TalkerIdentifiers tid, String payload)
	{
		this(tid, SID, payload);
	}

	/* This constructor is needed, as HDGSenetnece is extended by HDTSentence */
	HdgSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);

		super.parseHeading(HEADING_IDX);
		super.parseDeviation(DEVIATION_IDX, DEVIATION_EW_IDX);
		super.parseVariation(VARIATION_IDX, VARIATION_EW_IDX);
		headingData.setReferenceType(BearingData.ReferenceType.MAGNETIC);
		super.headingToHashMap();
	}
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import jdk.javadoc.internal.doclets.formats.html.markup.Head;
import marinelib.EmptyDataFieldException;
import marinelib.TalkerIdentifiers;
import marinelib.data.BearingData;
import marinelib.data.BearingData.ReferenceType;
import marinelib.data.HeadingData;
import marinelib.data.HeadingSupplier;

import java.util.HashMap;

/**
 *
 * @author jos
 */
abstract public class HeadingSentence extends AbstractSentence
		implements HeadingSupplier
{
	
	HashMap<BearingData.ReferenceType, HeadingData> headings = new HashMap<>();
	HeadingData headingData = new HeadingData();


	
	HeadingSentence(TalkerIdentifiers tid, String sid, String payload)
	{
		super(tid, sid, payload);
	}

	void parseHeading (int headingIdx)
	{
		try
		{
			headingData.parseString(fields[headingIdx]);
		}
		catch (ArrayIndexOutOfBoundsException 
				| IllegalArgumentException
				| StringIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.HeadingSentence.parseVariation()"
			//			+ ex.getMessage());
		}
	}

	void parseType (int typeIdx)
	{
		try
		{
			headingData.parseReferenceType(fields[typeIdx].charAt(0));
		}
		catch (IllegalArgumentException
				| ArrayIndexOutOfBoundsException
				| StringIndexOutOfBoundsException ex)
		{
			//System.out.println("marinelib.sentence.HeadingSentence.parseType()"
			//			+ ex.getMessage());
		}

		
	}
	
	void parseDeviation (int deviationIdx, int ewIdx)
	{
		try
		{
			char ew  = fields[ewIdx].charAt(0);
			headingData.parseDeviation(fields[deviationIdx], ew);
		}
		catch (ArrayIndexOutOfBoundsException 
				| IllegalArgumentException
				| StringIndexOutOfBoundsException ex)
		{
		//	System.out.println("marinelib.sentence.HeadingSentence.parseVariation()"
		//				+ ex.getMessage());	
		}
	}

	void parseVariation (int variationIdx, int ewIdx)
	{
		try
		{
			char ew  = fields[ewIdx].charAt(0);
			headingData.parseVariation(fields[variationIdx], ew);
		}
		catch (ArrayIndexOutOfBoundsException 
				| IllegalArgumentException
				| StringIndexOutOfBoundsException ex)
		{
		}
	}

	void headingToHashMap ()
	{
		headings.put(headingData.getReferenceType(), headingData);
	}
	
	@Override
	public HeadingData getHeadingData ()
	{
		return headingData;
	}


	@Override
	public HeadingData getHeadingData(BearingData.ReferenceType type)
			throws EmptyDataFieldException, UnsupportedOperationException
	{
		if (headings.isEmpty())
			throw new EmptyDataFieldException("No heading available");

		HeadingData headingData = headings.get(type);
		if (headingData != null)
			return headingData;

		throw new EmptyDataFieldException("No heading of that type available");

	}

	@Override
	public String getInfoString()
	{
		return tid + sid + ": " + headingData;
	}

}

package marinelib.sentence;

import marinelib.TalkerIdentifiers;
import marinelib.data.RotData;

public class RotSentence extends GenericSentence
{
    public static final String SID = "ROT";

    static final int ROT_IDX = 0;

    RotData rot = new RotData();

    public RotSentence(TalkerIdentifiers tid, String payload)
    {
        super(tid, SID, payload);

        try
        {
            rot.parseRot(fields[ROT_IDX]);
        }
        catch (IllegalArgumentException
                | ArrayIndexOutOfBoundsException
                | StringIndexOutOfBoundsException ex)
        {

        }
    }



    @Override
    public String getInfoString ()
    {
        return tid + sid + ": " + rot;
    }
}

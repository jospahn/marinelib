/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.sentence;

import marinelib.TalkerIdentifiers;

/**
 * The GGA Sentence representing position, time and additional fix informataion.
 * 
 * @author jos
 */
public class GgaSentence extends PositionSentence 
{
	public static final String SID = "GGA";
	
	boolean isFix = false;
	boolean isDifferential = false;
	byte satsInView;
	
	/**
	 * 
	 * @param tid
	 * @param payload
	 */
	public GgaSentence(TalkerIdentifiers tid, String payload) 
    {
        super(tid, SID, payload);	

		super.parsePosition(1, 2, 3, 4);
		super.parseTime(0);

		if (fields[5].equals("1"))
			isFix = true;
		if (fields[5].equals("2"))
		{
			isFix = true;
			isDifferential = true;
		}
		satsInView = Byte.parseByte(fields[6]);
	}

	/**
	 * 
	 * @return 
	 */
	@Override
	public String getInfoString ()
	{
		return "[GGA: " 
			+ position
			+ (isFix ? (isDifferential ? " DGPS" : " GPS")  : " No") + " Fix"
			+ " @ " + time
			+ "]"
		;
	}

}

/*
                                                      11
        1         2       3 4        5 6 7  8   9  10 |  12 13  14   15
        |         |       | |        | | |  |   |   | |   | |   |    |
 $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh<CR><LF>

 Field Number: 
  1) Universal Time Coordinated (UTC)
  2) Latitude
  3) N or S (North or South)
  4) Longitude
  5) E or W (East or West)
  6) GPS Quality Indicator,
     0 - fix not available,
     1 - GPS fix,
     2 - Differential GPS fix
  7) Number of satellites in view, 00 - 12
  8) Horizontal Dilution of precision
  9) Antenna Altitude above/below mean-sea-level (geoid) 
 10) Units of antenna altitude, meters
 11) Geoidal separation, the difference between the WGS-84 earth
     ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level
     below ellipsoid
 12) Units of geoidal separation, meters
 13) Age of differential GPS data, time in seconds since last SC104
     type 1 or 9 update, null field when DGPS is not used
 14) Differential reference station ID, 0000-1023
 15) Checksum 
*/

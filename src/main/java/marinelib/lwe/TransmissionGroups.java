package marinelib.lwe;

import java.net.InetAddress;

public enum TransmissionGroups {
    MISC("239.192.0.1", 6001),
    TGTD("239.192.0.2", 6002),
    SATD("239.192.0.3", 6003),
    NAVD("239.192.0.4", 6004),
    VDRD("239.192.0.5", 6005),
    RCOM("239.192.0.6", 6006),
    TIME("239.192.0.7", 6007),
    PROP("239.192.0.8", 6008);

        private InetAddress ip;
        private Integer port;

        private TransmissionGroups(String ip, Integer port)
        {
            try {
                this.ip = InetAddress.getByName(ip);
                this.port = port;
            } catch (Exception e) {

            }
        }

        public InetAddress getInetAdress()
        {
            return this.ip;
        }

        public Integer getPort()
        {
            return this.port;
        }

}

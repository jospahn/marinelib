package marinelib;

import marinelib.listener.LineListener;
import javax.swing.event.EventListenerList;


public abstract class SentenceInputReader
{
	private final EventListenerList listeners = new EventListenerList();
	
	
	/**
	 * Listeners will be notified by an event when a line has been 
	 * read.
	 * @param <T>
	 * @param t
	 * @param listener 
	 */
	public <T extends LineListener> void addListener (Class<T> t, T listener)
	{
		listeners.add(t, listener);
	}
	
	public <T extends LineListener> void removeListener (Class<T> t, T listener)
	{
		listeners.remove(t, listener);
	}
	
	/* 
	 * Notify all registered listeners by iterating over listener
	 * list an passing the LineEvent e
	 */
	<T extends LineListener> void notifyListeners (Class<T> t, LineEvent e)
	{
		for (LineListener l : listeners.getListeners(t))
		{
			l.update(e);
		}
	}

	/**
	 * Start the LineListener thread. When running it will chekc for new 
	 * characters each time invoked.
	 */
	public abstract void start();

	/**
	 * Stop the line listener thread. The thread will die when invoke the
	 * next time (So it will be called one time more after this method call)
	 */ 
	public abstract void stop();
	
	public abstract void readLine();

	public abstract boolean isStarted();
}

package marinelib;

import marinelib.listener.DataListener;
import marinelib.listener.SentenceListener;
import marinelib.listener.LineListener;
import marinelib.sentence.GenericSentence;
import marinelib.sentence.SentenceTypesEnum;
import marinelib.sentence.AbstractSentence;
import java.util.regex.Pattern;
import javax.swing.event.EventListenerList;

/**
 * When receiving a line event, data will be validated and parsed. If an NMEA
 * sentence was found a corresponding snentence Object will be instantiated, 
 * registered listeners will be notified and passed the sentence object.
 * 
 * @author jos
 */
public class SentenceParser implements LineListener
{
	/* list of listeners for Nmea Sentences */
	private final SentenceListenerList sentenceListeners = new SentenceListenerList();
	/* list of listeners for non nmea data */
	private final EventListenerList dataListeners = new EventListenerList();

	// ToDO: Implement way to switch if checksum is ignored
	private boolean ignoreChecksum = true;

	@Override
	public void update(LineEvent e) 
	{
		/* Get the line form the event and validate it. */
		//String line = e.getLine();
		String blocks[] = e.getLine().split(Pattern.quote("\\"));
		SentenceValidator validator;
		int indexOfSentenceBlock=0;
		int i = 0;
			
		/* If we don't have at least one block, something went wrong.
		   So we can return here. Also we don't want to enter the validator loop
		   as the array size is checked at the end of the loop. This is done, 
		   as the validator cant be checked at the head
		*/
		if (blocks.length == 0)
			return;
		
		do 
		{
			//System.out.println("marinelib.SentenceParser.update() blocks[" + i + "]: " +  blocks[i]);
			validator = new SentenceValidator(blocks[i]);
			if (validator.isSentence())
				indexOfSentenceBlock = i;
			else
			{
			//Check Tag blocks
			// Check LWE header
			}
			i++;	
		} while ( i < blocks.length && !validator.isSentence());
		
		
		if (validator.isSentence() && (ignoreChecksum | validator.hasValidCheckSum()))	// Yes, we received NMEA data
		{
			AbstractSentence sentence;
			TalkerIdentifiers tid = null;
			try 
			{
				tid = TalkerIdentifiers.valueOf(validator.getTalkerId());
			}
			catch (IllegalArgumentException ex)
			{
				
			}

			try
			{
				SentenceTypesEnum sentenceType = SentenceTypesEnum.valueOf(validator.getSentenceId());				
				sentence = sentenceType.getSentence(tid, validator.getPayload());
				notifySentenceListener(sentence.getSid(), new SentenceEvent(this, sentence, blocks[indexOfSentenceBlock], e.getPortId()));
			}
			catch (IllegalArgumentException ex)
			{
				sentence = new GenericSentence(tid, validator.getSentenceId(), validator.getPayload());
				notifySentenceListener("", new SentenceEvent(this, sentence, blocks[indexOfSentenceBlock], false, e.getPortId()));
			}
		}
		else
		{
			notifyDataListeners(e);
		}
	}

	public void addSentenceListener(SentenceListener listener, String sid)
	{
		sentenceListeners.add(listener, sid);
	}
	
	public void removeSentenceListener(SentenceListener listener, String sid)
	{
		sentenceListeners.remove(listener, sid);
	}
	
	public void notifySentenceListener(String sid, SentenceEvent e)
	{
		sentenceListeners.resetCursor();
		while (sentenceListeners.hasNext(sid))
		{
			sentenceListeners.getSentenceListener().update(e);
		}
	}

	public void addDataListener (DataListener listener)
	{
		dataListeners.add(DataListener.class, listener);
	}
	
	public void removeDataListener (DataListener listener)
	{
		dataListeners.remove(DataListener.class, listener);
	}
	
	/* 
	 * Notify all registered listeners by iterating over listener
	 * list an passing the LineEvent e
	 */
	private void notifyDataListeners (LineEvent e)
	{
		for (LineListener l : dataListeners.getListeners(DataListener.class))
		{
			l.update(e);
		}
	}
}

package marinelib;

import java.io.BufferedReader;
import java.io.IOException;
import marinelib.listener.LineListener;


public class SentenceFileInputReader extends SentenceInputReader
{
	final BufferedReader reader;					// this will be the input

	public SentenceFileInputReader(BufferedReader reader)
	{
		this.reader = reader;
	}

	public void readLine()
	{
	    String line;
	    try
		{
			if ((line =reader.readLine()) != null)
			{
				notifyListeners(LineListener.class, new LineEvent(this, line, new PortIdentifier("File")));
			} else {
				reader.close();
			}

        } catch (IOException ex)
        {
        }
	}

	/**
	 * Start the LineListener thread. When running it will chekc for new 
	 * characters each time invoked.
	 */
	@Override
	public void start()
	{
		String line;
		try
		{
			while ((line =reader.readLine()) != null)
			{
				notifyListeners(LineListener.class, new LineEvent(this, line, new PortIdentifier("File")));
			}
			reader.close();
		} catch (IOException ex)
		{
		}
	}

	@Override
	public void stop()
	{
		try
		{
			reader.close();
		} catch (IOException ex)
		{
		}
	}

	@Override
	public boolean isStarted()
	{
		//ToDO: Wie isStarted() beim SentenceFileInputReader richtig implementieren?
		return true;
	}
}

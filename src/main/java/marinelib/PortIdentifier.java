/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib;

/**
 * This class is used to give information about a port for data IO. 
 * It can be referenced by an event or data type. The advantage over refencing
 * a port object itslef is, that it will persist - information will be available 
 * even if the port was closed!
 * 
 * This tyoe shall be improved or extended by subclasses, now it's just quickly
 * implemented for referencing it in the parses stuff.
 * 
 * @author Jole
 */
public class PortIdentifier
{
	final String name;	

	/**
	 * Constructor initializes instance with a name.
	 * @param name
	 */
	public PortIdentifier (String name) 
	{
		this.name = name;
	}

	@Override
	public String toString ()
	{
		return "Port name: " + name ;
	}
}

package marinelib.data;

public class RotData
{
    private float rot;

    public float getRot()
    {
        return rot;
    }

    public void setRot(float rot)
    {
        this.rot = rot;
    }

    public void parseRot(String rot) throws IllegalArgumentException
    {
        try
        {
            this.rot = Float.parseFloat(rot);
        }
        catch (NumberFormatException | NullPointerException e)
        {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString ()
    {
        return rot + " °/min";
    }

}

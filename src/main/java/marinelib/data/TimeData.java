/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

/**
 * Represents a time of day conatining of hours, minutes, seconds and as an
 * option milliseconds. Allows to parse time from string as received by GGA and GLL
 * @author jos
 */
public class TimeData
{
	private byte hours;
	private byte minutes;
	private byte seconds;
	private byte millis;
	private boolean isUTC;
	private boolean isValid = false; // true after correct parsing

	/**
	 * Parse a string containing a time in the format hhmmss or hhmmss.ddd 
	 * @param time
	 * @throws IllegalArgumentException 
	 */
	public void parseTime (String time) throws IllegalArgumentException
	{
		int length =time.length();
		
		if (length > 5)
		{
			parseHours(time.substring(0, 2));
			parseMinutes(time.substring(2, 4));
			parseSeconds(time.substring(4, 6));
		}
		if (time.length() > 7)
			parseMillis(time.substring(7));
	}

	/**
	 * 
	 * @param hours a number from 0 to 24 as string
	 * @throws IllegalArgumentException  if the input is not representing a correct number
	 */
	public void parseHours ( String hours) throws IllegalArgumentException
	{
		try
		{
			int value = Integer.parseInt(hours);
			if (value < 0 || value > 23)
				throw new IllegalArgumentException("Argument must be 0 to 23");

			this.hours = (byte) value;
			this.isValid = true;
			
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 
	 * @return  hours
	 */
	public byte getHours ()
	{
		return hours;
	}
	
	/**
	 * 
	 * @param minutes a number from 0 t 59 as String
	 * @throws IllegalArgumentException 
	 */
	public void parseMinutes ( String minutes) throws IllegalArgumentException
	{
		try
		{
			int value = Integer.parseInt(minutes);
			if (value < 0 || value > 59)
				throw new IllegalArgumentException("Argument must be 0 to 59");

			this.minutes = (byte) value;
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 
	 * @return minutes 
	 */
	public byte getMinutes ()
	{
		return minutes;
	}

	/**
	 * 
	 * @param seconds a number from 0 t 59 as String
	 * @throws IllegalArgumentException 
	 */
	public void parseSeconds ( String seconds ) throws IllegalArgumentException
	{
		try
		{
			int value = Integer.parseInt(seconds );
			if (value < 0 || value > 59)
				throw new IllegalArgumentException("Argument must be 0 to 59");
			this.seconds  = (byte) value;
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 
	 * @return seconds 
	 */
	public byte getSeconds  ()
	{
		return seconds ;
	}
	
	/**
	 * Parse a string representing additional milliseconds
	 * @param millis a number from 0 to 99 as String
	 * @throws IllegalArgumentException 
	 */
	public void parseMillis ( String millis ) throws IllegalArgumentException
	{
		try
		{
			int value = Integer.parseInt(millis );
			if (value < 0 || value > 99)
				throw new IllegalArgumentException("Argument must be 0 to 99");
			this.millis  = (byte) value;
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * 
	 * @return milliseconds 
	 */
	public byte getMillis  ()
	{
		return seconds ;
	}

	/**
	 * 
	 * @param isUTC ture if a UTC time shall be represented
	 */
	public void setUTC(boolean isUTC)
	{
		this.isUTC = isUTC;
	}

	/**
	 * 
	 * @return true if a UTC time is represented 
	 */
	public boolean isUTC ()
	{
		return isUTC;
	}

	/**
	 * 
	 * @return true if hours have correctly been parsed 
	 */
	public boolean isValid ()
	{
		return isValid;
	}

	@Override
	public String toString ()
	{
		if (isValid == true)
			return (hours < 10 ? "0" : "") + hours + ":" 
				+ (minutes < 10 ? "0" : "") + minutes + ":" 
				+ (seconds < 10 ? "0" : "") + seconds   
				+ (millis > 0 ? "." + millis : "") 
				+ (isUTC ? " UTC" : "");
		else
			return ("::");
	}
}

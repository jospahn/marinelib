/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

import marinelib.EmptyDataFieldException;

/**
 *
 * @author jos
 */
public interface CourseSupplier
{
	public CourseData getCourseData() throws EmptyDataFieldException;

	public default CourseData getCourseData(BearingData.ReferenceType referenceType)
			throws EmptyDataFieldException, UnsupportedOperationException
	{
		throw new UnsupportedOperationException();
	}

}

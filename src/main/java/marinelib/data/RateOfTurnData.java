/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

/**
 *
 * @author jos
 */
public class RateOfTurnData
{
	private float rot;

	public void parseRateOfTurn (String rot)
	{
		try
		{
			this.rot = Float.parseFloat(rot);
		}
		catch (NumberFormatException | NullPointerException e)
		{
                    System.out.println("marinelib.data.HeadingData.parseHeading(): Exception " + e.getMessage());
                }
	}

	public float getRateOfTurn ()
	{
		return rot;
	}

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

/**
 *
 * @author jos
 */
public class HeadingData extends BearingData
{
	private float variation; 	// Missweisung Deklination, abhängig von der Position
	private float deviation; 	// Fehler bedingt durch Umgebungsbedingungen des Magnetkompasses

	public void parseDeviation (String deviation, char ew) throws IllegalArgumentException
	{
		try
		{
			this.deviation = Float.parseFloat(deviation);
		}
		catch (NumberFormatException | NullPointerException  e)
		{
			throw new IllegalArgumentException();
		}
		this.deviation = applyEW(ew, this.deviation);
	}

	public void parseVariation (String variation, char ew) throws IllegalArgumentException
	{
		try
		{
			this.variation = Float.parseFloat(variation);
		}
		catch (NumberFormatException | NullPointerException  e)
		{
			throw new IllegalArgumentException();
		}
		this.variation = applyEW(ew, this.variation);
	}


	public float getVariation()
	{
		return variation;
	}

	public float getDeviation()
	{
		return deviation;
	}


	@Override
	public String toString ()
	{
	    return super.toString() + " Heading";
	}

}
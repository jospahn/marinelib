package marinelib.data;

import marinelib.util.SpeedUnits;

/**
 * Conatins speed data: Speed, lateral speed and identifies if it is over ground or through water
 * @author jos
 */
public class SpeedData
{
    double speed =  0;
    SpeedType speedType = SpeedType.GROUND_SPEED;
    boolean valid = false;

    /**
     * Parses speed given in knots and konverts ist to m/s for internal storage
     * @param speed given as String like 12.3
     * @throws IllegalArgumentException if speed=null or empty, not a number or if unit unrecognized
     */
    public void parseString (String speed) throws IllegalArgumentException
    {
        parseString(speed, 'N');
    }

    /**
     * Parses speed and converts ist to m/s for internal storage if needed.
     * @param speed given as String like 12.3
     * @param unit K=km/h, M=m/s, N=knots, S=mph
     * @throws IllegalArgumentException if speed=null or empty, not a number or if unit unrecognized
     */
    public void parseString (String speed, char unit) throws IllegalArgumentException {
        double temp;
        if (speed == null)
            throw new IllegalArgumentException("Empty field");

        try {
            temp = Double.parseDouble(speed);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("speed must be a number like xx.x");
        }

        switch (unit) {
            case 'K':
                this.speed = SpeedUnits.kmhToMs(temp);
                break;
            case 'M':
                this.speed = temp;
                break;
            case 'N':
                this.speed = SpeedUnits.knToMs(temp);
                break;
            case 'S':
                this.speed = SpeedUnits.mphToMs(temp);
                break;
            default:
                throw new IllegalArgumentException("Speed unit not recognized");
        }
    }


    /**
     *
     * @return speed in m/s
     */
    public double getValue ()
    {
        return speed;
    }

    public SpeedType getSpeedType ()
    {
        return this.speedType;
    }

    public void setSpeedType (SpeedType speedType)
    {
        this.speedType = speedType;
    }

    public void setValid (boolean valid)
    {
        this.valid = valid;
    }

    public boolean isValid()
    {
        return valid;
    }

    @Override
    public String toString ()
    {
        return speed + "m/s " + speedType.getName();
    }

    public enum SpeedType
    {
        GROUND_SPEED("speed over Ground"),
        WATER_SPEED("speed through water"),
        LONGITUDINAL_GROUND_SPEED("longitudinal speed over Ground"),
        LONGITUDINAL_WATER_SPEED("longitudinal speed through water"),
        TRANSVERSE_GROUND_SPEED("transverse speed over ground"),
        TRANSVERSE_WATER_SPEED("transverse speed through water");

        private String name;

        SpeedType(String name)
        {
            this.name = name;
        }

        public String getName ()
        {
            return name;
        }

    }
}

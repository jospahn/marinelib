/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;


import marinelib.EmptyDataFieldException;

/**
 *
 * @author jos
 */
public interface SpeedSupplier
{
	public SpeedData getSpeedData() throws EmptyDataFieldException;

	public default SpeedData getSpeedData(SpeedData.SpeedType type)
			throws EmptyDataFieldException, UnsupportedOperationException
	{
	    throw new UnsupportedOperationException();
	}
}

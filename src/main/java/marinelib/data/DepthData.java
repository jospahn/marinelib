/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

/**
 *
 * @author jos
 */
public class DepthData
{
	private float depth;
	private float offsetKeel;
	private float offsetWater;

	public float getDepth()
	{
		return depth;
	}

	public void setDepth(float depth)
	{
		this.depth = depth;
	}

	public float getOffsetKeel()
	{
		return offsetKeel;
	}

	public void setOffsetKeel(float offsetKeel)
	{
		this.offsetKeel = offsetKeel;
	}

	public float getOffsetWater()
	{
		return offsetWater;
	}

	public void setOffsetWater(float offsetWater)
	{
		this.offsetWater = offsetWater;
	}

	/**
	 * The depth in m will be returned as 12.3m  
	 * @return 
	 */
	@Override
	public String toString ()
	{
		return depth + " m";
	}
}

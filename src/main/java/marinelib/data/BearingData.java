/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.data;

/**
 *
 * @author jos
 */
public class BearingData
{
	protected float value;
	private ReferenceType referenceType = ReferenceType.UNKNOWN;

	public void parseString (String bearing) throws IllegalArgumentException
	{
		try
		{
			this.value = Float.parseFloat(bearing);
		}
		catch (NumberFormatException | NullPointerException e)
		{
			throw new IllegalArgumentException();
		}
	}

	public float applyEW (char ew, float value) throws IllegalArgumentException
	{
		if (ew == 'E' && value < 0)
		{
			value *= -1;
			return value;
		}
		if (ew == 'W' && value >= 0)
		{
			value *= -1;
			return value;
		}
		throw new IllegalArgumentException("Unknown direction indicator");
	}

	public void parseReferenceType (char type) throws IllegalArgumentException
	{
		if (type == 'T')
		{
		    this.referenceType = ReferenceType.TRUE;
			return;
		}
		if (type == 'M')
		{
			this.referenceType = ReferenceType.MAGNETIC;
			return;
		}
		throw new IllegalArgumentException();
	}

	public void setReferenceType (ReferenceType type)
	{
		this.referenceType = type;
	}

	public float getValue()
	{
		return value;
	}

	public ReferenceType getReferenceType ()
	{
		return this.referenceType;
	}


	@Override
	public String toString ()
	{
		return value + "° " + this.referenceType.getName();
	}

	public enum ReferenceType
	{
		TRUE("true"),
		MAGNETIC("magnetic"),
		UNKNOWN("");

		private String name;

		ReferenceType(String name)
		{
			this.name = name;
		}

		public String getName ()
		{
			return name;
		}
	}

}
package marinelib.data;

import marinelib.util.SpeedUnits;

/**
 * Direction, angle and speed of wind, relative and/or true.
 * Uses m/s as speed unit internally.
 * @author spahn
 */
public class WindData 
{
	private double speed;
	private double direction;
	private double angle;
	private boolean trueWind = false;	//true if true wind, false if relative Wind
	
	private boolean validDirection = false; 
	private boolean validSpeed = false; 
	private boolean validAngle = false;
	
	/**
	 * Parses speed and converts ist to m/s if needed.
	 * @param speed given as String like 12.3
	 * @param unit K=km/h, M=m/s, N=knots, S=mph
	 * @throws IllegalArgumentException if speed=null or empty, not a number or if unit unrecognized
	 */
	public void parseSpeed (String speed, char unit) throws IllegalArgumentException
	{
		double temp;
		if (speed == null)
			throw new IllegalArgumentException("Empty field");

		try
		{
			temp = Double.parseDouble(speed);
		}
		catch (NumberFormatException ex)
		{
			throw new IllegalArgumentException("speed must be a number like xx.x");
		}

		switch (unit)
		{
		case 'K' :
			this.speed = SpeedUnits.kmhToMs(temp);
			this.validSpeed = true;
			break;
		case 'M' :
			this.speed = temp;
			this.validSpeed = true;
			break;
		case 'N' :
			this.speed = SpeedUnits.knToMs(temp);
			this.validSpeed = true;
			break;
		case 'S' :
			this.speed = SpeedUnits.mphToMs(temp);
			this.validSpeed = true;
			break;
		default:
			throw new IllegalArgumentException("Speed unit not recognized");
		}
	}
	
	/**
	 * Set if wind data represent true wind or relative wind
	 * @param trueWind  true if true
	 */
	public void setTrueWind (boolean trueWind)
	{
		this.trueWind = trueWind;
	}

	
	/**
	 * Parse wind direction from String
	 * @param dir
	 */
	public void parseDirection (String dir) throws IllegalArgumentException
	{
		if (dir == null) 
			throw new IllegalArgumentException("Empty field");
		try
		{
			this.direction = Double.parseDouble(dir);
		}
		catch (NumberFormatException ex)
		{
			throw new IllegalArgumentException("dir must be a number like xx.x");
		}
		this.validDirection = true;
	}

	/**
	 * Parse angle of wind relative to heading line from String.
	 * @param angle
	 * @throws IllegalArgumentException 
	 */
	public void parseAngle (String angle) throws IllegalArgumentException 
	{
		if (angle == null) 
			throw new IllegalArgumentException("Empty field");
		try
		{
			this.angle = Double.parseDouble(angle);
		}
		catch (NumberFormatException ex)
		{
			throw new IllegalArgumentException("angle must be a number like xx.x");
		}
		this.validAngle = true;
	}

	/**
	 * Set side of wind angle (-/+ from left or right)
	 * @param lr L for left, R for right
	 * @throws IllegalArgumentException 
	 */
	public void parseSide (char lr) throws IllegalArgumentException
	{
		if (lr == 'L' && this.angle >= 0)
		{
			this.angle *= -1;
			return;
		} 
		
		if (lr == 'R' && this.angle < 0)
		{
			this.angle *= -1;
			return;
		} 

		// if we reach here, neither L nor R was given as argument
		throw new IllegalArgumentException("Unrecognized direction letter");
	}

	public double getSpeed() {
		return speed;
	}

	public boolean isValidDirection() {
		return validDirection;
	}

	public boolean isValidSpeed() {
		return validSpeed;
	}

	public boolean isValidAngle() {
		return validAngle;
	}

	public double getDirection() {
		return direction;
	}

	public double getAngle() {
		return angle;
	}

	public boolean isTrueWind() {
		return trueWind;
	}

	@Override
	public String toString ()
	{
			return (isTrueWind() ? "wahrer" : "relativer") + " Wind "
				+ (isValidSpeed() ?getSpeed() : "?") + "m/s @ "
				+ (isValidDirection()? getDirection() : (isValidAngle() ? getAngle() : "?"))
				+ "°" + (isValidAngle() ? " von der Vorauslinie" : "")
			;
	}

}
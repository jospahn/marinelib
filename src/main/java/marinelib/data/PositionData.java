package marinelib.data;

/**
 * Representation of coordinates with paring and access methods.
 * @author Jole
 */
public class PositionData 
{
	private double latitude;
	private double longitude;
	private boolean valid = false;		// true after correct parsing
	private boolean latValid = false;
	private boolean lonValid = false;
	private boolean nsHemiValid = false;
	private boolean ewHemiValid = false;

	/**
	 * Is a correctly parsed, valid postion available?
	 * @return true if valid
	 */
	public boolean isValid()
	{
		return valid;
	}


	/**
	 *  
	 * @return Latitude in Degree Minute Format, decimal representation xxxx.xx
	 */
	public double getLatitude()
	{
		return latitude;
	}

	/**
	 *  
	 * @return Longitude in Degree Minute Format, decimal representation xxxx.xx
	 */
	public double getLongitude()
	{
		return longitude;
	}

	/**
	 *
	 * @return Latitude in decimal degrees
	 */
	public double getLatDezimal()
	{
		int deg = (int) this.latitude / 100;
		double dec = this.latitude % 100;

		dec = dec / 60;

		return (double) deg + dec;
	}

	/**
	 *
	 * @return Longitude in decimal degrees
	 */
	public double getLonDezimal()
	{
		int deg = (int) this.longitude / 100;
		double dec = this.longitude % 100;

		dec = dec / 60;

		return (double) deg + dec;
	}

	/**
	 * 
	 * @return Latitude string representation as 53°10.123 N
	 */
	public String latString ()
	{
		if (valid)
		{
			double ll = (latitude < 0 ? latitude * -1 : latitude );
			int deg = (int) ll / 100;
			double min = ll % 100;
			return  deg + "°" + String.format("%07.4f", min)  
					+ " " + (latitude < 0 ? "S" : "N" );
		}
		else
			return "";
	}

	/**
	 * 
	 * @return Longitude string representation as 53°10.123 N
	 */
	public String lonString ()
	{
		if (valid)
		{
			double ll = (longitude < 0 ? longitude * -1 : longitude );
			int deg = (int) ll / 100;
			double min = ll % 100;
			return  deg + "°" + String.format("%07.4f", min)  
					+ " " + (longitude < 0 ? "W" : "E" );
		}
		else
			return "";
	}
	
	/**
	 * Parse a string containing a latitude in decimal format xx.xxxx
	 * @param lat String representing a decimal latitude
	 * @throws IllegalArgumentException 
	 */
	public void parseLat (String lat ) throws IllegalArgumentException
	{
		try
		{
			this.latitude = Double.parseDouble(lat);
			latValid = true;
			valid = latValid & lonValid & nsHemiValid & ewHemiValid; 
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Parse a string containing a longitude in decimal format xx.xxxx
	 * @param lon String representing a decimal longitude
	 * @throws IllegalArgumentException 
	 */
	public void parseLon (String lon) throws IllegalArgumentException
	{
		try
		{
			this.longitude = Double.parseDouble(lon);
			lonValid = true;
			valid = latValid & lonValid & nsHemiValid & ewHemiValid; 
		}
		catch (NumberFormatException e)
		{
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Parse the hemisphere of latitude or longitude.
	 * @param hemi char N, S, W, E (or O)
	 * @throws IllegalArgumentException 
	 */
	public void parseHemi (char hemi) throws IllegalArgumentException
	{
		switch (hemi)
		{
		case 'N':
			if (latitude > 0)
				latitude *= 1;
			nsHemiValid = true;	
			break;
		case 'S':
			if (latitude > 0)
				latitude *= -1;
			nsHemiValid = true;	
			break;
		case 'E':
		case 'O':
			if (longitude > 0)
				longitude *= 1;
			ewHemiValid = true;	
			break;
		case 'W':
			if (longitude > 0)
				longitude *= -1;
			ewHemiValid = true;	
			break;
		default:
			throw new IllegalArgumentException();
		}
		valid = latValid & lonValid & nsHemiValid & ewHemiValid; 
	}

	/**
	 * A string representation of lat, lon and hemisphere
	 * @return 
	 */
	@Override
	public String toString()
	{
		return ("Lat " + latString() + ", Lon " + lonString());
	}
}

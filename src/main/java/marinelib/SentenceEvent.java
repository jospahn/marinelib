package marinelib;

import marinelib.sentence.AbstractSentence;

public class SentenceEvent extends LineEvent
{
	private final AbstractSentence sentence;
	private final Boolean isKnown ;

	public SentenceEvent(Object source, AbstractSentence sentence, PortIdentifier port)
	{
		this(source, sentence, null, true, port);
	}

	public SentenceEvent(Object source, AbstractSentence sentence, String line, PortIdentifier port)
	{
		this(source, sentence, line, true, port);
	}

	public SentenceEvent(Object source, AbstractSentence sentence, String line, boolean isKnown, PortIdentifier port)
	{
		super(source, line, port );
		this.sentence = sentence;
		this.isKnown = isKnown;
	}

	
	public AbstractSentence getSentence()
	{
		return sentence;
	}

	public boolean isKnown ()
	{
		return isKnown;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.util;

/**
 *
 * @author jos
 */
public class CheckSum
{
	public static int calcChecksum (String string)
	{
		int checkSum = 0;
		for (int i = 0; i < string.length(); i++ )
			checkSum ^= string.charAt(i);
		
		return checkSum;
	}

	public static String getHexChecksum (String string)
	{
		String cs;	
		cs = Integer.toHexString(calcChecksum(string)).toUpperCase();
		if (cs.length() < 2)
			return 0 + cs;
		return cs; 
	}

}

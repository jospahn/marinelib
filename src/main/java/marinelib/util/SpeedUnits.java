/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marinelib.util;

/**
 * This util class provides methods to convert from and to different speed units.
 * @author spahn
 */
public class SpeedUnits 
{
	public static double msToKmh ( double value)
	{
		return (Math.round(value * 36)) / 10.0;
	}

	public static double kmhToMs ( double value )
	{
		// Devide by 0.36 instead of 3.6, than round and than devide by 10 -> round to 1 decimal
		return (Math.round(value / 0.36)) / 10.0;
	}

	public static double msToMph ( double value)
	{
		return (Math.round(value * 22.3694)) / 10.0;
	}
	
	public static double mphToMs ( double value)
	{
		return (Math.round(value / 0.223694)) / 10.0;
	}
	
	public static double msToKn ( double value)
	{
		return (Math.round(value * 19.4384)) / 10.0;
	}

	public static double knToMs ( double value)
	{
		// Devide by 0.19 instead of 1.9, than round and than devide by 10 -> round to 1 decimal
		return (Math.round(value / 0.194384)) / 10.0;
	}

	public static int msToBft ( double value)
	{
		if (value <0)
			value *= -1;

		if (value <= 0.2)
			return 0;
		if (value <= 1.5)
			return 1;
		if (value <= 3.3)
			return 2;
		if (value <= 5.4)
			return 3;
		if (value <= 7.9)
			return 4;
		if (value <= 10.7)
			return 5;
		if (value <= 13.8)
			return 6;
		if (value <= 17.1)
			return 7;
		if (value <= 20.7)
			return 8;
		if (value <= 24.4)
			return 9;
		if (value <= 28.4)
			return 10;
		if (value <= 32.6)
			return 11;
		
		return 12;
	}
}

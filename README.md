marinelib
=========

A parsing library for data sentences carrying navigational data on ships,
usually referred to as NMEA.
This library is 
**not based on NMEA0183 / IEC61162 standards**, as those are closed source. 
Instead it relies on information freely available on the
internet. So hopefully it is compatible with most common usage of sentences.
Also it contains some first attempts to work with NMEA like data over udp multicast
(aka LWE), but the implementation can't be comprehensive as not much information about 
the related standard (-450) is freely available yet.

 
